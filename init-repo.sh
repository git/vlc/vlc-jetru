#!/bin/sh

if [[ $# != 3 ]]
then
	echo "Usage: $0 <project> <student name> <your name>"
	exit 1
fi

echo "Creating new Git repo: $1-$2.git"
echo "Your git login is: $3 (please remove from config once the first push is done)"

git diff --quiet --exit-code
clean=$?
if [[ $clean != 0 ]]
then
	echo "Your gitosis repo isn't clean. (see git-diff)"
	exit 1
fi

git pull --rebase

if [[ $? != 0 ]]
then
	echo "Git pull failed"
	exit 1
fi

(
echo ""
echo "[repo $1-$2]"
echo "gitweb = yes"
echo "daemon = yes"
echo "description = $2's $1 repo"
echo "owner = $2"
echo ""
echo "[group $2]"
echo "writable = sandbox $1-$2"
echo "members = $2 $3"
) >> gitosis.conf

git commit -m "Add $1-$2.git"  gitosis.conf && git push

if [[ $? != 0 ]]
then
	echo "Changing the gitosis configuration failed"
	exit 1
fi

set -x
git clone git://git.videolan.org/$1.git /tmp/$1-$2-git
if [[ $? != 0 ]]
then
	echo "Git clone of $1.git failed."
	exit 1
fi
cd /tmp/$1-$2-git
git config remote.origin.url git@git.videolan.org:$1-$2.git
git push origin master:refs/heads/master
if [[ $? != 0 ]]
then
	echo "Git push of $1-$2.git failed."
	exit 1
fi
set +x
cd -
rm -rf /tmp/$1-$2-git

echo ""
echo "You now need to execute the following commands on skanda:"
echo 'find /home/git/repositories/'$1'-'$2'.git -type d -exec chmod a+rx \{\} \; -o -type f -exec chmod a+r \{\} \;'
echo 'touch /home/git/repositories/'$1'-'$2'.git/EXPORT'
